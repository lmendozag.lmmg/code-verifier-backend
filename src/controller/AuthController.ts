import { Delete, Get, Post, Put, Query, Route, Tags } from 'tsoa'
import { IAuthController } from './interfaces'
import { LogSuccess, LogError, LogWarning, LogInfo } from '../utils/logger'
import { IUser } from '../domain/interfaces/IUser.Interface'
import { IAuth } from '../domain/entities/IAuth.interface'

// ORM - Users Collection
import {
  registerUser,
  loginUser,
  logoutUser,
  getUserByID,
} from '../domain/orm/User.orm'
import { AuthResponse, ErrorResponse } from './types'

@Route('/api/auth')
@Tags('AuthController')
export class AuthController implements IAuthController {
  @Post('/register')
  public async registerUser(user: IUser): Promise<any> {
    let response: any = ''

    if (user) {
      LogSuccess(`[/api/auth/register] Register New User: ${user.email}`)
      await registerUser(user).then((r) => {
        LogSuccess(`[/api/auth/register] Created User: ${user.email}`)
        response = {
          message: `User created successfully: ${user.name}`,
        }
      })
    } else {
      LogWarning(`[/api/auth/register] Register needs User Entity`)
      response = {
        message: `User not Registered: Please, provide a User Entity to create one`,
      }
    }

    return response
  }

  @Post('/login')
  public async loginUser(auth: IAuth): Promise<any> {
    let response: AuthResponse | ErrorResponse | undefined

    if (auth) {
      LogSuccess(`[/api/auth/login] Logged In User: ${auth.email}`)
      let data = await loginUser(auth)

      response = {
        token: data.token,
        message: `welcome, ${data.user.name}`,
      }
    } else {
      LogWarning(
        `[/api/auth/login] Register needs Auth Entity (email && password)`
      )
      response = {
        error: '[AUTH ERROR]: Email & Password are needed',
        message: `Please, provide an email && password to login`,
      }
    }

    return response
  }

  /**
   * Enpoint to retreive the Users in the Collection "Users" of DB
   * Middleware: Validate JWT
   * In headers you must add the x-access-token with a valid JWT
   * @param {string} id Id of user to retreive (option)
   * @returns All user o user foung by ID
   */
  @Get('/me')
  public async userData(@Query() id: string): Promise<any> {
    let response: any = ''

    if (id) {
      LogSuccess(`[/api/users] Get User Data by ID: ${id}`)

      response = await getUserByID(id)
    }

    return response
  }

  @Post('/logout')
  public async logoutUser(): Promise<any> {
    let Response: any = ''
    // TODO: Close session of user
    throw new Error('Method not implemented')
  }
}
