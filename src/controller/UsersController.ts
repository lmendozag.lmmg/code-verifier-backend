import { Delete, Get, Post, Put, Query, Route, Tags } from 'tsoa'
import { IUserController } from './interfaces'
import { LogSuccess, LogError, LogWarning } from '../utils/logger'

// ORM - Users Collection
import {
  deleteUserByID,
  getAllUsers,
  getUserByID,
  createUser,
  updateUserByID,
} from '../domain/orm/User.orm'

@Route('/api/users')
@Tags('UserController')
export class UserController implements IUserController {
  /**
   * Enpoint to retreive the Users in the Collection "Users" of DB
   * @param {string} id Id of user to retreive (option)
   * @returns All user o user foung by ID
   */
  @Get('/')
  public async getUsers(
    @Query() page: number,
    @Query() limit: number,
    @Query() id?: string
  ): Promise<any> {
    let response: any = ''

    if (id) {
      LogSuccess(`[/api/users] Get User by ID: ${id}`)

      response = await getUserByID(id)
    } else {
      LogSuccess('[/api/users] Get All Users Request')

      response = await getAllUsers(page, limit)
    }

    return response
  }

  /**
   * Enpoint to delete the Users in the Collection "Users" of DB
   * @param {string} id Id of user to delete (option)
   * @returns message informing if deletion was correct
   */
  @Delete('/')
  public async deleteUser(@Query() id?: string): Promise<any> {
    let response: any = ''

    if (id) {
      LogSuccess(`[/api/users] Delete User by ID: ${id}`)

      await deleteUserByID(id).then((r) => {
        response = {
          message: `User with id ${id} deleted succesfully`,
        }
      })
    } else {
      LogWarning('[/api/users] Delete Users Request WITHOUT ID')

      response = {
        message: `Please provide an ID to remove from database`,
      }
    }

    return response
  }

  @Put('/')
  public async updateUser(@Query() id: string, user: any): Promise<any> {
    let response: any = ''

    if (id) {
      LogSuccess(`[/api/users] Update User By ID: ${id}`)
      await updateUserByID(id, user).then((r) => {
        response = {
          message: `User with id ${id} updated successfully`,
        }
      })
    } else {
      LogWarning(`[/api/users] Update User Request WITHOUT ID`)
      response = {
        message: `Please, provide an ID to update an existing user`,
      }
    }

    return response
  }
}
