import { userEntity } from '../entities/User.entity'
import { LogError, LogSuccess } from '../../utils/logger'
import { IUser } from '../interfaces/IUser.Interface'
import { IAuth } from '../entities/IAuth.interface'

// Environment variable
import dotenv from 'dotenv'

// BCRYPT for passwords
import bcrypt from 'bcrypt'

// JWT
import jwt from 'jsonwebtoken'
import { UserResponse } from '../types/UsersResponse.type'

// Configuration of environment variables
dotenv.config()

// Obtain Secret Key to generate JWT
const secret = process.env.SECRETKEY || 'MYSECRETKEY'

// CRUD
/**
 * Method to obtain all Users from Collection "Users" in Mongo Server
 */
export const getAllUsers = async (
  page: number,
  limit: number
): Promise<any[] | undefined> => {
  try {
    let userModel = userEntity()

    let response: any = {}

    // Search all users (using pagination)
    await userModel
      .find()
      .limit(limit)
      .skip((page - 1) * limit)
      .select('name email age')
      // .projection({ name: 1, email: 1, age: 1 })
      .exec()
      .then((users: IUser[]) => {
        response.users = users
      })

    // Count total documents in collection "Users"
    await userModel.countDocuments().then((total: number) => {
      response.totalPages = Math.ceil(total / limit)
      response.currentPage = page
    })

    return response
  } catch (error) {
    LogError(`[ORM ERROR]: Getting All Users: ${error}`)
  }
}

// - Get User By ID
export const getUserByID = async (id: string): Promise<any | undefined> => {
  try {
    let userModel = userEntity()

    // Search User by ID
    return await userModel.findById(id).select('name email age')
  } catch (error) {
    LogError(`[ORM ERROR]: Getting User by ID: ${error}`)
  }
}

// - Delete User By ID
export const deleteUserByID = async (id: string): Promise<any | undefined> => {
  try {
    let userModel = userEntity()

    // Delete User By ID
    return await userModel.deleteOne({ _id: id })
  } catch (error) {
    LogError(`[ORM ERROR]: Deleting User by ID: ${error}`)
  }
}

// - Create New User
export const createUser = async (user: any): Promise<any | undefined> => {
  try {
    let userModel = userEntity()

    // Create / Insert new User
    return await userModel.create(user)
  } catch (error) {
    LogError(`[ORM ERROR]: Creating User: ${error}`)
  }
}

// - Update User By ID
export const updateUserByID = async (
  id: string,
  user: any
): Promise<any | undefined> => {
  try {
    let userModel = userEntity()

    // Update User
    return await userModel.findByIdAndUpdate(id, user)
  } catch (error) {
    LogError(`[ORM ERROR]: Updating User ${id}: ${error}`)
  }
}

// Register User
export const registerUser = async (user: IUser): Promise<any | undefined> => {
  try {
    let userModel = userEntity()

    // Create / Insert new User
    return await userModel.create(user)
  } catch (error) {
    LogError(`[ORM ERROR]: Creating User: ${error}`)
  }
}

// Login User
export const loginUser = async (auth: IAuth): Promise<any | undefined> => {
  try {
    let userModel = userEntity()
    let userFound: IUser | undefined = undefined
    let token = undefined

    // Check if user exists by unique Email
    await userModel
      .findOne({ email: auth.email })
      .then((user: IUser) => {
        userFound = user
      })
      .catch((error) => {
        console.error(`[ERROR Authentication in ORM]: User Not Found`)
        throw new Error(
          `[ERROR Authentication in ORM]: User Not Found: ${error}`
        )
      })

    // Check if Password is Valid (compare with bcrypt)
    let validPassword = bcrypt.compareSync(auth.password, userFound!.password)

    if (!validPassword) {
      console.error(`[ERROR Authentication in ORM]: Password Not Valid`)
      throw new Error(`[ERROR Authentication in ORM]: Password Not Valid`)
    }

    // Generate JWT
    token = jwt.sign({ email: userFound!.email }, secret, {
      expiresIn: '2h',
    })

    return {
      user: userFound,
      token: token,
    }
  } catch (error) {
    LogError(`[ORM ERROR]: Login User: ${error}`)
  }
}

// Logout User
export const logoutUser = async (): Promise<any | undefined> => {
  // TODO: Not Implemented
}
