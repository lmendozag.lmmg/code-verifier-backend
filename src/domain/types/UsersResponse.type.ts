import { IUser } from '../interfaces/IUser.Interface'

export type UserResponse = {
  users: IUser[]
  totalPages: number
  currentPage: number
}
