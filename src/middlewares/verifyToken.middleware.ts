import jwt from 'jsonwebtoken'
import { Request, Response, NextFunction } from 'express'

import dotenv from 'dotenv'

// Config dotenv to read environment variables
dotenv.config()

const secret = process.env.SECRETKEY || 'MYSECRETKEY'

/**
 *
 * @param req { Request } Original request previous middleware of verification JWT
 * @param res { Response } Response to verification of JWT
 * @param next { NextFunction } Next function to be executed
 * @returns Errors of verfication or nex execution
 */
export const verifyToken = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  // Check HEADER from Request for 'x-access-token'
  let token: any = req.headers['x-access-token']

  // Verify if jwt is present
  if (!token) {
    return res.status(403).send({
      authenticationError: 'Missing JWT in request',
      message: 'Not authorized to consume this endpoint',
    })
  }

  // Verify the token obtained. We pass the secret
  jwt.verify(token, secret, (err: any, decoded: any) => {
    if (err) {
      return res.status(500).send({
        authenticationError: 'JWT verification failed',
        message: 'Failed to verify JWT token in request',
      })
    }

    // Execute Next Function -> Protected Routes will be executed
    next()
  })
}
