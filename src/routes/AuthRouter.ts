import express, { Request, Response } from 'express'
import { AuthController } from '../controller/AuthController'
import { LogInfo } from '../utils/logger'
import { IUser } from '../domain/interfaces/IUser.Interface'
import { IAuth } from '../domain/entities/IAuth.interface'

// BCRYPT for passwords
import bcrypt from 'bcrypt'

// MiddleWare
import { verifyToken } from '../middlewares/verifyToken.middleware'

// Body Parser (Read JSON from Body in Requests)
import bodyParser = require('body-parser')

// Middleware to read JSON in Body
let jsonParser = bodyParser.json()

// Router from express
let authRouter = express.Router()

authRouter
  .route('/register')
  // POST
  .post(jsonParser, async (req: Request, res: Response) => {
    let { name, email, password, age } = req?.body
    let hashedPassword = ''

    if (password && name && email && age) {
      // Obtain the pasword in request and cypher
      hashedPassword = bcrypt.hashSync(password, 8)

      let newUser: IUser = {
        name,
        email,
        password: hashedPassword,
        age,
      }

      // Controller Instance to execute method
      const controller: AuthController = new AuthController()

      // Obtain Response
      const response: any = await controller.registerUser(newUser)

      // Send to the client the response
      return res.status(200).send(response)
    } else {
      // Send to the client the response
      return res.status(400).send({
        message: "[ERROR User Data Missing]: User can't be registered.",
      })
    }
  })

authRouter
  .route('/login')
  // POST
  .post(jsonParser, async (req: Request, res: Response) => {
    let { email, password } = req?.body

    if (password && email) {
      // Controller Instance to execute method
      const controller: AuthController = new AuthController()

      let auth: IAuth = {
        email,
        password,
      }

      // Obtain Response
      const response: any = await controller.loginUser(auth)

      // Send to the client the response which includes the JWT to autorize requests
      return res.status(200).send(response)
    } else {
      // Send to the client the response
      return res.status(400).send({
        message: "[ERROR User Data Missing]: User can't be registered.",
      })
    }
  })

// Route Protectec by VERIFY TOKEN Middleware
authRouter
  .route('/me')
  .get(verifyToken, async (req: Request, res: Response) => {
    // Obtain the ID of user to check it's data
    let id: any = req?.query?.id

    if (id) {
      // Controller: Auth Controller
      const controller: AuthController = new AuthController()

      // Obtain response from Controller
      let response: any = await controller.userData(id)

      // if user is autorized
      return res.status(200).send(response)
    } else {
      return res.status(401).send({
        message: 'You are not authorized to perform this action',
      })
    }
  })
export default authRouter
