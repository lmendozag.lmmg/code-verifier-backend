import express, { Request, Response } from 'express'
import { UserController } from '../controller/UsersController'
import { LogInfo } from '../utils/logger'

// Body Parser to read BODY from requests
import bodyParser from 'body-parser'

// JWT Verifier Middleware
import { verifyToken } from '../middlewares/verifyToken.middleware'

// Middleware to read JSON in Body
let jsonParser = bodyParser.json()

// Router from express
let userRouter = express.Router()

// http://localhost:8000/api/users?id=660b64f781e0abacafe12671
userRouter
  .route('/')
  // Get:
  .get(verifyToken, async (req: Request, res: Response) => {
    // Obtain a Query Param (ID)
    let id: any = req?.query?.id

    // Pagination
    let page: any = req?.query?.page || 1
    let limit: any = req?.query?.limit || 10

    LogInfo(`Query Param: ${id}`)

    // Controller Instance to execute method
    const controller: UserController = new UserController()

    // Obtain Response
    const response: any = await controller.getUsers(page, limit, id)

    // Send to the client the response
    return res.status(200).send(response)
  })
  // DELETE:
  .delete(verifyToken, async (req: Request, res: Response) => {
    // Obtain a Query Param (ID)
    let id: any = req?.query?.id
    LogInfo(`Query Param: ${id}`)

    // Controller Instance to execute method
    const controller: UserController = new UserController()

    // Obtain Response
    const response: any = await controller.deleteUser(id)

    // Send to the client the response
    return res.status(200).send(response)
  })
  // PUT:
  .put(verifyToken, async (req: Request, res: Response) => {
    // Obtain a Query Param (ID)
    let id: any = req?.query?.id
    let name: any = req?.query?.name
    let email: any = req?.query?.email
    let age: any = req?.query?.age

    LogInfo(`Query Param: ${id}, ${name}, ${email}, ${age}`)

    // Controller Instance to execute method
    const controller: UserController = new UserController()

    let user = {
      name: name,
      email: email,
      age: age,
    }

    // Obtain Response
    const response: any = await controller.updateUser(id, user)

    // Send to the client the response
    return res.status(200).send(response)
  })

// Export UsersRouter
export default userRouter

/**
 *
 * Get Documents => 200 OK
 * Creation Document => 201 OK
 * Deletion of Document => 200 (Entity) / 204 (No Return)
 * Update of Documents => 200 (Entity) / 204 (no Return)
 */
