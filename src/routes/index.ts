/**
 * Root Router
 * Redirections to Routers
 */
import express, { Request, Response } from 'express'
import helloRouter from './HelloRouter'
import { LogError, LogInfo } from '../utils/logger'
import userRouter from './UserRouter'
import { serve } from 'swagger-ui-express'
import authRouter from './AuthRouter'

// Server instance
let server = express()

// Router instence
let rootRouter = express.Router()

// Activate for requests to http://localhost:8000/api

// GET: http://localhost:8000/api/
rootRouter.get('/', (req: Request, res: Response) => {
  LogError('GET: http://localhost:8000/api/')
  // Send Hello World
  res.send('Welcome')
})

// Redirections to Routers & Controllers
server.use('/', rootRouter) // http://localhost:8000/api/
server.use('/hello', helloRouter) // http://localhost:8000/api/hello --> HelloRouter
// Add more routes to the app
server.use('/users', userRouter) // http://localhost:8000/api/users --> UserRouter
// Auth routes
server.use('/auth', authRouter) // http://localhost:8000/api/auth --> AuthRouter

export default server
